#include <iostream>
#include <ctype.h>
#include "otp.h"
#include "caesar.h"
#include "vigenere.h"
#include "hill.h"
#include "otp.cpp"
#include "caesar.cpp"
#include "vigenere.cpp"
#include "hill.cpp"

bool isDigit(std::string str) {
    for (char c : str) {
        if (!std::isdigit(c)) return false;
    }
    return true;
}

bool filterOtp(std::string message, std::string key){
    return isDigit(message) || isDigit(key) ? true : false;
}

bool filterCaesar(std::string message, std::string key) {
    return isDigit(key);
}

int main()
{
    setlocale(LC_ALL, "");

    std::string choice;

    //std::cout << hillEncrypt("act", "gybnqkurp");


    
    do {
        //break;
        std::cout << std::endl;
        std::cout << "Escolha seu algoritmo\n"
            << "\t1 - otp   2 - caesar  3 - vigenere    4 - hill    5 - sair\n"
            << "> ";
        std::cin >> choice;

        if (choice == "1" || choice == "otp") {
            std::string option;
            std::cout << "\t 1 - Encriptar \t 2 - Desencriptar\n"
                << "> ";
            std::cin >> option;

            if (option != "1" && option != "2") continue;


            std::string otp_message;
            std::cout << "Digite sua mensagem: ";
            std::cin >> otp_message;

            std::string otp_key;
            std::cout << "Digite sua chave: ";
            std::cin >> otp_key;

            if(!filterOtp(otp_message, otp_key)) continue;

            if (option == "1")
                std::cout << "Sua mensagem encriptada: " << otpEncrypt(std::stoi(otp_message), std::stoi(otp_key)) << "\n";

            if (option == "2")
                std::cout << "Sua mensagem desencriptada: " << otpDecrypt(std::stoi(otp_message), std::stoi(otp_key)) << "\n";
        }

        if (choice == "2" || choice == "caesar") {
            std::string option;
            std::cout << "\t 1 - Encriptar \t 2 - Desencriptar\n"
                << "> ";
            std::cin >> option;

            if (option != "1" && option != "2") continue;

            std::string caesar_message;
            std::cout << "Digite sua mensagem: ";
            std::cin >> caesar_message;

            std::string caesar_key;
            std::cout << "Digite sua chave: ";
            std::cin >> caesar_key;

            if (!filterCaesar(caesar_message, caesar_key)) continue;

            if (option == "1")
                std::cout << "Sua mensagem encriptada: " << caesarEncrypt(caesar_message, std::stoi(caesar_key)) << "\n";

            if (option == "2")
                std::cout << "Sua mensagem desencriptada: " << caesarDecrypt(caesar_message, std::stoi(caesar_key)) << "\n";
        }

        if (choice == "3" || choice == "vigenere") {
            std::string option;
            std::cout << "\t 1 - Encriptar \t 2 - Desencriptar\n"
                << "> ";
            std::cin >> option;

            if (option != "1" && option != "2") continue;

            std::string vigenere_message;
            std::cout << "Digite sua mensagem: ";
            std::cin >> vigenere_message;

            std::string vigenere_key;
            std::cout << "Digite sua chave: ";
            std::cin >> vigenere_key;

            if (option == "1")
                std::cout << "Sua mensagem encriptada: " << vigenereEncrypt(vigenere_message, vigenere_key) << "\n";

            if (option == "2")
                std::cout << "Sua mensagem desencriptada: " << vigenereDecrypt(vigenere_message, vigenere_key) << "\n";
        }

        if (choice == "4" || choice == "hill") {
            std::string option;
            std::cout << "\t 1 - Encriptar\n"
                << "> ";
            std::cin >> option;

            if (option != "1") continue;

            std::string hill_message;
            std::cout << "Digite sua mensagem: ";
            std::cin >> hill_message;

            std::string hill_key;
            std::cout << "Digite sua chave: ";
            std::cin >> hill_key;

            if (option == "1")
                std::cout << "Sua mensagem encriptada: " << hillEncrypt(hill_message, hill_key) << "\n";
                // hillEncrypt(hill_message, hill_key);
        }

    } while (choice != "sair" && choice != "5");


    // Exemplos

    // std::cout << "Valor: 164 - Chave: 205 - Resultado: " << otpEncrypt(164, 205) << "\n";

    // std::cout << "Valor: 105 - Chave: 205 - Resultado: " << otpEncrypt(105, 205) << "\n";

    //std::cout << "Valor: " << caesarEncrypt("abcdefghijk lmnopqrstu vwxyz", 1) << "\n";

    // std::cout << "Valor: " << caesarEncrypt("bcdefghijkl mnopqrstuv wxyza", 30) << "\n";

    // std::cout << "Valor: " << caesarDecrypt("bcdefghijkl mnopqrstuv wxyza", 1) << "\n";

    // std::cout << "Valor: " << caesarEncrypt("Hoje e quinta feira", 3) << "\n";

    //std::cout << "Valor: " << caesarEncrypt("ABCDEFGHIJK LMNOPQRSTU VWXYZ", 1) << "\n";

    //std::cout << "Valor: " << caesarEncrypt("ABCDEFGHIJK LMNOPQRSTU VWXYZ", 26) << "\n";

    //std::cout << "Valor: " << caesarEncrypt("ABCDEFGHIJK LMNOPQRSTU VWXYZ", 30) << "\n";

    //std::cout << "Valor: " << caesarEncrypt("bcdefghijkl mnopqrstuv wxyza", 25) << "\n";

    /*std::cout << vigenereEncrypt("abelha", "tes") << "\n"
        << vigenereDecrypt("tfwels", "tes") << "\n";*/

    //std::cout << hillEncrypt("act", "gybnqkurp");
}