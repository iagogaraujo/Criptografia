#include "caesar.h"

/*
	CASOS ESPECIAIS = ESPA�O, LETRAS MAIUSCULAS
*/

std::string caesarEncrypt(std::string message, int key) {

	key = key % 26;
	char limitChar;
	int charValue;

	// Para cada char dentro da string Mensagem
	for (int i = 0; i < message.size(); i++) {
		// Define o limite para char minusculo
		limitChar = 'z';

		// Ignora caso o char seja espaço
		if (message[i] == ' ')
			continue;

		// Verifica se o char é maiusculo
		if (message[i] >= 'A' && message[i] <= 'Z')
			limitChar = 'Z';

		// Pega o valor ASCII do char atual
		charValue = message[i];

		// Rotaciona o valor
		charValue += key;

		// Caso o char tenha passado do limite
		if (charValue > limitChar)
			// Subtraia o valor ASCII por 26
			charValue -= 26;

		// O valor atual do vetor é trocado pelo formatado
		message[i] = charValue;
	}
	return message;
}

std::string caesarDecrypt(std::string messageEncrypted, int key) { return caesarEncrypt(messageEncrypted, key + 24); };