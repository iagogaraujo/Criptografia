#include "otp.h"
#include <vector>
#include <iostream>
#include <string>
#include <cmath>

int otpEncrypt(int number, int key) {
	std::vector<int> numberBinary;
	std::vector<int> keyBinary;
	std::vector<int> result;

	numberBinary = decimalToBinary(number);
	keyBinary = decimalToBinary(key);
	
	// Os dois vetores tem o mesmo tamanho?
	//if (numberBinary.size() != keyBinary.size()) return 0;
	if (numberBinary.size() != keyBinary.size()) {
		while (numberBinary.size() < keyBinary.size())
			numberBinary.insert(numberBinary.begin(), 0);
		while (keyBinary.size() > numberBinary.size())
			keyBinary.insert(keyBinary.begin(), 0);
	}

	// Faz o loop e confere se os dois n�mero s�o diferentes (XOR)
	for (int i = 0; i < numberBinary.size(); i++)
		numberBinary[i] != keyBinary[i] ?
		result.push_back(1) : result.push_back(0);

	// Retorna convers�o para decimal
	return binaryToDecimal(result);
}

int otpDecrypt(int numberEncrypted, int key)
{
	return otpEncrypt(numberEncrypted, key);
}

std::vector<int> decimalToBinary(int value)
{
	std::vector<int> result;
	while (value != 0) {
		// std::cout << "Value: " << value << "\n";
		result.insert(result.begin(), value % 2);
		value /= 2;
	}

	return result;
}

int binaryToDecimal(std::vector<int> value){
	int result = 0;

	for (int i = value.size(); i--; )
		result = result + value[i] * std::pow(2, value.size() - i - 1);

	return result;
}

