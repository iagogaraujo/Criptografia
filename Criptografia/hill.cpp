#include "hill.h"
#include <string>
#include <cmath>
#include <iostream>
#include <math.h>

// Retorna string encryptografada	

// std::string hillEncrypt(std::string message, std::string key) {
// 	if (key.size() < std::pow(message.size(), 2)) return "Inválido!";

// 	// Definindo resultado
// 	std::string result = message;
// 	// Definindo tamanho da matrix a partir do tamanho da mensagem
// 	int matrix_size = message.size();

// 	// Matrix usada sendo um vetor de vetores
// 	std::vector< std::vector <char> > matrix;

// 	int charValue = 0;

// 	/* 
// 		CRIA A MATRIX DA CHAVE
// 	*/
// 	// Loop para cada char dentro de message
// 	for (int y = 0; y < message.size(); y++) {
// 		// Cria um vetor temporário de char
// 		std::vector<char> temp;
// 		// Vai empurrar ao temp a fileira atual da chave
// 		for (int x = 0; x < message.size(); x++) temp.push_back(key[x + (y * message.size())]);

// 		// Empurra fileira para matrix
// 		matrix.push_back(temp);
// 	}

// 	int counter = 0;

// 	/*
// 		FAZ ENCRIPTAÇÃO
// 	*/

// 	// Loop entre cada fileira dentro da matrix
// 	for (int i = 0; i < matrix.size(); i++) {
// 		int total = 0;
// 		int counter2 = 0;
// 		// Loop para cada elemento da fileira atual
// 		for (int b : matrix[i]) {

// 			/*
// 				FAZ ENCRIPTAÇÃO COM ELEMENTO ATUAL
// 			*/

// 			int messageInt = message[counter2];
// 			int thisCount = ((b - 'a') * (messageInt - 'a')) % 26;
// 			total += thisCount;
// 			counter2++;
// 		}
// 		total = total % 26;
// 		result[counter] = total + 'a';
// 		total = 0;
// 		counter++;
// 	}

// 	return result;
// }


// Retorna string descriptografada
std::string hillDecrypt(std::string messageEncrypted, std::string key);


std::string hillEncrypt(std::string message, std::string key) {

	std::string return_string;

	if (key.size() != 4) return "Inválido!";

	int key_array[2][2] = 	{	{key[0] - 'a', key[1] - 'a'},
								{key[2] - 'a', key[3] - 'a'}
							};
	
	// std::cout << "#########################\n";
	// for (int i = 0; i < 2; i++) {
	// 	for (int b = 0; b < 2; b++) {
	// 		std::cout << key_array[i][b] << " ";
	// 	}
	// 	std::cout << "\n";
	// }
	// std::cout << "#########################\n";

	const int result_size = std::ceil(message.size() / 2);

	int result[result_size][2];

	int localresult[2];

	for (int i = 0; i < message.size(); i+=2) {
		// Próximo elemento = (i + 1) % message.size -> Z -> A

		// Definindo vetor do resultado local

		// std::cout << "Letra atual: " << message[i] << " Valor: " << message[i]-'a' << " Próxima letra: " << message[(i + 1) % message.size()] << " Valor: " << message[(i + 1) % message.size()] -'a' ;
		// std::cout << "\n";

		localresult[0] = ( (key_array[0][0] * (message[i] - 'a')) + (key_array[0][1] * (message[(i + 1) % message.size()] -'a')) );

		// std::cout << "Conta: " << key_array[0][0] << " * " << message[i] - 'a' 
		// 	<< " + " << key_array[0][1] << " * " << message[i] - 'a'
		// << " Resultado: " << localresult[0] << "\n";

		localresult[1] = ( (key_array[1][0] * (message[i] - 'a')) + (key_array[1][1] * (message[(i + 1) % message.size()] -'a')) );


		// Passando resultado para result
		for (int _ = 0; _ < 2; _++) result[i / 2][_] = localresult[_];

	}

	for (int i = 0; i < result_size; i++){
		for (int b = 0; b < 2; b++){
			// std::cout << result[i][b] << " ";

			return_string.push_back(((result[i][b]) % 26) + 'a');
		}
		// std::cout << "\n";
	}


return return_string;

}