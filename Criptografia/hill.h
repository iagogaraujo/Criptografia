#include <vector>
#include <string>

// Retorna string encryptografada	
std::string hillEncrypt(std::string message, std::string key);

// Retorna string descriptografada
std::string hillDecrypt(std::string messageEncrypted, std::string key);