#include <string>

// Retorna string encryptografada
std::string caesarEncrypt(std::string message, int key);

// Retorna string descriptografada
std::string caesarDecrypt(std::string messageEncrypted, int key);