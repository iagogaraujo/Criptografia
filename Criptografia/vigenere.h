#include <string>

// Retorna string encryptografada	
std::string vigenereEncrypt(std::string message, std::string key);

// Retorna string descriptografada
std::string vigenereDecrypt(std::string messageEncrypted, std::string key);