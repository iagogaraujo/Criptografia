#include <vector>

// Retorna valor int encriptografado
int otpEncrypt(int number, int key);

// Retorna valor int descriptografado
int otpDecrypt(int numberEncrypted, int key);

// Retorna valor bin�rio em std::vector<int> a partir de um int
std::vector<int> decimalToBinary(int value);

// Retorna valor decimal a partir de um std::vector<int>
int binaryToDecimal(std::vector<int> value);