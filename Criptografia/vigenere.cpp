#include <string>
#include <iostream>
#include <cstring>
#include "caesar.h"
#define fa(i, a) for (int i = 0; i < (a); i++)

std::string charToString(char c) { std::string s(1, c); return s; }

std::string lowerEverything(std::string input) {
	fa(i, input.size())  if (input[i] = std::tolower(input[i])); 
	return input;
}

std::string upEverything(std::string input) {
	fa(i, input.size())  if (input[i] = std::toupper(input[i])); 
	return input;
}
std::string vigenereEncrypt(std::string message, std::string key) {

	// 	char asciiKey = key[i % key.size()] > 'a' && key[i % key.size()] < 'z' ? 'a' : 'A';

	message = lowerEverything(message);
	key = lowerEverything(key);

	// Define variável resposta
	std::string result;
	// fa = loop entre i até X
	// Empurra resultado para result
	// Aplica caesar
	// key [i % key.size()] = ajustar chave para tamanho do vetor
	// BUG: deve haver uma verficação para caso o valor atual da chave seja maiusculo para subtraiar por - 'A'
	fa(i, message.size()) result.push_back(caesarEncrypt(charToString(message[i]), key[i % key.size()] - 'a')[0]);

	return upEverything(result);
}

std::string vigenereDecrypt(std::string messageEncrypted, std::string key) {

	messageEncrypted = lowerEverything(messageEncrypted);
	key = lowerEverything(key);

	std::string result;
	// Mesma lógica
	fa(i, messageEncrypted.size())
		result.push_back( ( ( ( messageEncrypted[i] - key[i % key.size()] ) + 26 ) % 26 ) + 'a');

	return upEverything(result);
};